<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicer extends CI_Controller
{
    /**
     * @var Settings_model
     */
    public $settings;

    /**
     * @var Servicer_model
     */
    public $servicer;

    private $ant = 0;

    /**
     *  Constructor
     */
    public function __construct()
    {
        parent::__construct();

        if (!$this->ant) {
            $this->load->model('servicer_model', 'servicer');
        }
    }

    /*
     */
    public function run()
    {
        header('Content-Type: application/x-javascript');

        $ant = $this->ant;

        if ($ant) {
        echo "window['MarketGidLoadGoods{$ant}']([
['','123456','1','Тестовый заголовок','Тестовое описание','0','','10.00 грн',' ','TeStHaSh',{i:'http://localhost:8000/img1.jpg'}],
['','654321','1','Второй тестовый заголовок','Второе тестовое описание','0','','20.00 грн',' ','tEsThAsH',{i:'http://localhost:8000/img2.jpg'}],
['','111222','1','Третий тестовый заголовок','Третье тестовое описание','0','','30.00 грн',' ','tEsThAsH',{i:'http://localhost:8000/img3.jpg', l:'//link-from-goodsblock.com/111222'}]
]);";
            exit();
        } else {
            $result = $this->servicer->getScript();

            echo $result;
        }
    }
}

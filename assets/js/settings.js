$(document).ready(function(){
    function changeSettings(name, value) {
        var data = {};
        data[name] = value;

        var request = $.ajax({
            url: "/settings/up",
            data: data,
            dataType: 'json',
            method: 'GET'
        });

        request.done(function(response){
            if (response.message) {
                alertInfo(response.message);
            }
        });
    }

    $('#settings input').change(function(){
        var key = $(this).attr('name');
        var val = $(this).val();

        if ($(this).attr('type') == 'checkbox') {
            val = $(this).prop('checked');
        }

        changeSettings(key, val);
    });

    function alertInfo(message) {
        var notification = $('#notification');
        if (notification.size() == 0) {
            notification = $('<div />', {
                'id' : 'notification',
                'class' : 'alert alert-info',
                'style' : 'position: absolute;margin: auto 0;width: 50%;bottom: 0;left: 25%;display:none;'
            });

            $('body').append(notification);
        }

        notification.html(message);
        notification.slideDown();

        $('body').append(notification)
        setTimeout(function () {
            $('#notification').slideUp(function(){

                $('#notification').remove();
            });
        }, 2000);
    }
});
<?php
class Settings_model extends CI_Model
{
    const SETTINGS_FILE = 'settings.json';

    public $settings = [];

    public function __construct()
    {
        parent::__construct();

        $this->initSettings();
    }

    /**
     * @throws Exception
     */
    private function initSettings()
    {
        $this->settings = (array) json_decode($this->getSettingsFile());
    }

    public function flush()
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . self::SETTINGS_FILE;
        if (!is_really_writable($path)) {
            throw new Exception('file: "' . $path . '" must be writable!');
        }

        file_put_contents($path, json_encode($this->all()));
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        if (is_array($key)) {
            foreach($key as $key => $value) {
                $this->set($key, $value);
            }
        }
        $this->settings[$key] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->settings;
    }

    /**
     * @param $key
     * @param $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return isset($this->settings[$key]) ? $this->settings[$key] : $default;
    }

    /**
     * @return string
     * @throws Exception
     */
    private function getSettingsFile()
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . self::SETTINGS_FILE;
        if (!is_really_writable($path)) {
            throw new Exception('file: "' . $path . '" must be writable!');
        }

        $file = file_get_contents($path);

        return $file;
    }

    public function __destruct()
    {
        $this->flush();
    }
}
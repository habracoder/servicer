<?php
class Servicer_model extends CI_Model
{
    const TYPE_NEWS = 'news';
    const TYPE_GOODS = 'goods';
    const TYPE_COMPOSITE = 'composite';

    public $title;
    public $content;
    public $date;
    private $params;
    private $query;
    private $data;
    private $count;
    private $informer;
    private $teasers = [];

    public function __construct()
    {
        parent::__construct();

        $this->data     = new stdClass();
        $this->params   = new stdClass();
        $this->informer = new stdClass();

        $this->initUri()
            ->initQuery()
            ->initInformer()
            ->initTeasers();
    }

    public function getScript()
    {
        $informer = $this->getInformer();

        $tpl = "var _mgq = _mgq || [];
_mgq.push([':method', [\n:teasers\n],{
 awc:{},
 dt:'desktop'
}]);
:rejectMethod_mgqp();";

        $teasersString = '';

        foreach ($this->getTeasers() as $teaser) {
            if ($this->getType() == self::TYPE_GOODS) {
                $teasersString .= sprintf("['%s',{i:'" . $this->initImage($teaser->image_link) . "'}],\n", implode("', '", [
                    $teaser->url,
                    $teaser->id,
                    '1',
                    $teaser->title,
                    isset($teaser->advert_text) ? $teaser->advert_text : '',
                    '1',
                    '',
                    isset($teaser->price) ? $this->formatPrice($teaser->price) : '',
                    isset($teaser->price_old) ? $this->formatPrice($teaser->price_old) : '',
                    'f3z9iJP_G1SN85k2BaTDHGNiBXjhSO6ySZmNok-j6Kxn4TfESCMw3oaTTmnC67za' . rand(10000, 99999),
                ]));
            } else {
                $teasersString .= sprintf(
                    "['%s',{i:'" . $this->initImage($teaser->image_link) . "'}],\n", implode("', '",
                    [
                        $teaser->url,
                        $teaser->id,
                        '1',
                        $teaser->title,
                        isset($teaser->advert_text) ? $teaser->advert_text : '',
                        '',
                        'f3z9iJP_G1SN85k2BaTDHGNiBXjhSO6ySZmNok-j6Kxn4TfESCMw3oaTTmnC67za' . rand(10000, 99999),
                    ]
                ));
            }
        }

        $teasersString = trim($teasersString, ",\n");

        $result = strtr($tpl, [
            ':teasers' => $teasersString,
            ':method' => $this->getInformerMethod(),
            ':main_informer_id' => $informer->parent ? $informer->parent : $informer->uid,
            ':rejectMethod' => $this->getRejectMethod()
        ]);

        return $result;
    }

    private function getRejectMethod()
    {
        if ($this->getInformer()->class == 'composite') {
            $ident = $this->getInformer()->parent;
            $typePrefix = 'C';
        } else {
            $ident = $this->getInformer()->uid;
            $typePrefix = strtoupper(substr($this->getType(), 0, 1));
        }

        if ($this->settings->get('rejectTeaser')) {
            return sprintf(
                "_mgq.push(['MarketGid%sReject%s']);\n",
                $typePrefix,
                $ident
            );
        }

        return '';
    }

    /**
     * Возвращает метод ответа с тизерами
     * @return string
     */
    private function getInformerMethod()
    {
        $domain = 'marketgid';
        preg_match('/(tovarro|lentainform|mgid)/', $_SERVER['HTTP_HOST'], $match);

        if (isset($match[1])) {
            $domain = $match[1];
        }

        switch ($domain) {
            case 'tovarro':
                $prefix = 'Tovarro';
                break;
            default:
                $prefix = 'MarketGid';
                break;
        }

        if ($this->getInformer()->class == self::TYPE_COMPOSITE && (int) $this->getInformer()->parent > 0) {
            $dayOfWeek = date('N') > 5 ? 'holyday' : 'weekday' ;
            $result = $this->db->select('type')->from('tickers_composite_default')
                ->where([
                    'composite' => (int) $this->getInformer()->parent,
                    'day_of_week' => $dayOfWeek
                ])->get()->row();
            if ($result && $result->type != $this->getType()) {
                return  $prefix . 'RedirectComposite' . $this->getInformer()->parent;
            }
        }

        switch ($this->getType()) {
            case self::TYPE_GOODS:
                $method = $prefix . 'LoadGoods' . $this->getInformer()->id;
                break;
            case self::TYPE_NEWS:
                $method = $prefix . 'LoadNews' . $this->getInformer()->id;
                break;
            default:
                $method = $prefix . 'RedirectComposite' . $this->getInformer()->parent;
                break;
        }

        return $method;
    }

    /**
     * @return string
     */
    public function getTickerDefault()
    {
        $type = $this->getType();

        $informer = $this->getInformer();
        if ($informer->class == self::TYPE_COMPOSITE && (int) $informer->parent > 0) {
            $dayOfWeek = date('N') > 5 ? 'holyday' : 'weekday' ;
            $result = $this->db->select('type')->from('tickers_composite_default')
                ->where([
                    'composite' => (int) $informer->parent,
                    'day_of_week' => $dayOfWeek
                ])->get()->row();
            if ($result) {
                $type = $result->type;
            }
        }

        return $type;
    }

    public function formatPrice($price)
    {
        $lang = 'ru';
        if ($lang == 'ru' && $price) {
            $price = ($price / 25) . ' грн.';
        } else {
            $price = ' ';
        }

        return $price;
    }

    /**
     * @param $src
     * @return string
     */
    private function initImage($src)
    {
        if ($this->settings->get('replaceImages', false)) {
            $imageSettings = (array)$this->getData()->mcimage;
            $width = $imageSettings['size-width'];
            $height = $imageSettings['size-height'];
            $src = 'https://imgg.marketgid.net' . '/image/' . rand(1000, 9999) .
                '/' . $width . 'x' . $height . '.jpg';
        }
        return $src;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return (int) $this->params->uid;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->params->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->params->type;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return (int) $this->params->page;
    }

    /**
     * @return $this
     */
    public function initInformer()
    {
        $table = $this->getType() == self::TYPE_GOODS ? 'g_blocks' : 'tickers';
        $this->informer = $this->db
            ->select('*')
            ->where('uid', $this->getUid())
            ->from($table)
            ->get()
            ->row();

        $this->initConstructorTemplate($this->informer->constructor_template);

        return $this;
    }

    private function initTeasers()
    {
        $table = $this->getType() == self::TYPE_GOODS ? 'g_hits_1' : 'news_1';
        $where = ['droped = 0'];
        if ($this->getType() == self::TYPE_GOODS) {
            $where[] = 'draft = 0';
        }
        $query = $this->db->query(strtr(
            "select * from :table where :where order by rand() limit :limit",
            [
                ':table' => $table,
                ':where' => implode(' and ', $where),
                ':limit' => $this->getCount()
            ]
        ));

        $this->teasers = $query->result();
    }

    /**
     * @param $json
     * @return $this
     */
    private function initConstructorTemplate($json)
    {
        $data = json_decode($json);

        $this->data = (object)$data;
        $this->count = (int)$this->data->rows * (int)$this->data->cols;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return $this
     */
    private function initUri()
    {
        preg_match('/(fs\/)?u([0-9]+)\/([0-9]+)\/([0-9]+)/', $_SERVER['REQUEST_URI'], $match);
        $this->params->fs   = (boolean) $match[1];
        $this->params->uid  = (int)$match[2];
        $this->params->id   = (int)$match[3];
        $this->params->page = (int)$match[4];

        preg_match('/[a-z]{2}\-([a-z]{1})/', $_SERVER['HTTP_HOST'], $typeMatch);
        $this->params->type = $typeMatch[1] == 'g' ? 'goods' : 'news';
        if (isset($_COOKIE['mg_type'])) {
            $this->params->type = $_COOKIE['mg_type'];
        }

        if ($this->settings->get('teasersType', self::TYPE_GOODS) != $this->getType()) {
            $type = $this->settings->get('teasersType', self::TYPE_GOODS);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getTeasers()
    {
        return $this->teasers;
    }

    /**
     * @return $this
     */
    private function initQuery()
    {
        parse_str($_SERVER['QUERY_STRING'], $params);
        $this->query = (object) $params;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getParam($name, $default = null)
    {
        return isset($this->query->$name) ? $this->query->$name : $default;
    }

    /**
     * @return stdClass
     */
    public function getInformer()
    {
        return $this->informer;
    }

    public function getData()
    {
        return $this->data;
    }
}

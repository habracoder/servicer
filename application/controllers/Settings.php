<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{
    /**
     * @var Settings_model
     */
    public $settings;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
	public function index()
	{
        $settings = $this->settings->all();


        $this->load->view('settings', $settings);
	}

    /**
     * @throws Exception
     */
    public function up()
    {
        $json = [];

        $params = $_GET;
        foreach ($params as $key => $val) {
            $this->settings->set($key, $this->clearValue($val));
        }

        $this->settings->flush();
        $json['message'] = 'Настройки сохранены';

        echo json_encode($json);
    }

    /**
     * @param $val
     * @return bool|int
     */
    public function clearValue($val)
    {
        $val = $val === 'true' ? true : $val;
        $val = $val === 'false' ? false : $val;
        $val = is_numeric($val) ? (int)$val : $val;

        return $val;
    }
}

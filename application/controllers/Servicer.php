<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicer extends CI_Controller
{
    /**
     * @var Settings_model
     */
    public $settings;

    /**
     * @var Servicer_model
     */
    public $servicer;

    private $ant = 0;

    /**
     *  Constructor
     */
    public function __construct()
    {
        parent::__construct();

        if (!$this->ant) {
            $this->load->model('servicer_model', 'servicer');
        }
    }

    /**
     * Скрипт отработки
     */
    public function run()
    {
        header('Content-Type: application/x-javascript');

        echo $this->servicer->getScript();
    }
}


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/assets/img/favicon.ico">

    <title>Narrow Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">


    <script src="/assets/js/jquery-2.1.4.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/settings.js"></script>
</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="#">Home</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Servicer local settings</h3>
    </div>
    <form class="form-horizontal" id="settings">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Картинки</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <label>
                        <input name="replaceImages" <?=$replaceImages ? ' checked=checked': ''?> type="checkbox"> Заменять изображения на фейковые
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Тип ответа</label>
            <div class="col-sm-10">
                <div class="radio">
                    <label>
                        <input name="teasersType" <?=$teasersType == 'news' ? ' checked=checked': ''?> type="radio" value="news"> Новостной
                    </label>
                    <label>
                        <input name="teasersType" <?=$teasersType == 'goods' ? ' checked=checked': ''?> type="radio" value="goods"> Товарный
                    </label>
                    <label>
                        <input name="teasersType" <?=$teasersType == 'redirect' ? ' checked=checked': ''?> type="radio" value="redirect"> Редирект
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Блокинг</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <label>
                        <input name="rejectTeaser" type="checkbox" <?=$rejectTeaser ? ' checked=checked': ''?> > Добавить кнопку блокировки тизера
                    </label>
                </div>
            </div>
        </div>
    </form>

</div> <!-- /container -->

<footer class="footer">
    <div class="container">
        <p>&copy; Servicer 2015</p>
    </div>
</footer>
</body>
</html>